use crate::{
    ray::Ray,
    vec3::{Point, Vec3},
};

pub struct Camera {
    origin: Point,
    ll_corner: Point,
    horizontal: Vec3,
    vertical: Vec3,

    u: Vec3,
    v: Vec3,
    _w: Vec3,

    lens_radius: f32,
}
impl Camera {
    pub fn init(
        look_from: &Point,
        look_at: &Point,
        vup: &Vec3,
        vfov: f32,
        aspect_ratio: f32,
        aperture: f32,
        focus_dist: f32,
    ) -> Self {
        let theta = vfov.to_radians();
        let h = (theta / 2.0).tan();

        let viewport_height = 2.0 * h;
        let viewport_width = viewport_height * aspect_ratio;

        let w = (*look_from - *look_at).unit();
        let u = (*vup).cross(w).unit();
        let v = w.cross(u);

        let origin = *look_from;
        let horizontal = focus_dist * viewport_width * u;
        let vertical = focus_dist * viewport_height * v;
        let ll_corner = origin - horizontal / 2.0 - vertical / 2.0 - focus_dist * w;

        let lens_radius = aperture / 2.0;

        return Self {
            origin,
            ll_corner,
            horizontal,
            vertical,
            u,
            v,
            _w: w,
            lens_radius,
        };
    }

    pub fn get_ray(&self, u: f32, v: f32) -> Ray {
        let rd = self.lens_radius * Vec3::rand_in_unit_disk();
        let offset = self.u * rd.x() + self.v * rd.y();

        Ray::new(
            self.origin + offset,
            self.ll_corner + u * self.horizontal + v * self.vertical - self.origin - offset,
        )
    }
}
