use super::*;
use crate::{objects::RayHit, ray::Ray};

use rand::prelude::*;
use rgb::RGB;

pub struct Dielectric {
    index_of_refrac: f32,
}
impl Dielectric {
    pub fn new(index_of_refrac: f32) -> Self {
        Self { index_of_refrac }
    }
}
impl Material for Dielectric {
    fn scatter(
        &self,
        ray: &Ray,
        hit: &RayHit,
        attenuation: &mut RGB<f32>,
        scattered: &mut Ray,
    ) -> bool {
        let mut rng = thread_rng();

        *attenuation = RGB::new(1.0, 1.0, 1.0);

        let refraction_ratio = hit
            .is_front()
            .expect("Dielectric material does not know if ray hit is front or not")
            .then(|| 1.0 / self.index_of_refrac)
            .unwrap_or(self.index_of_refrac);

        let unit_direction = ray.direction().unit();
        let cos_theta = (-unit_direction).dot(*hit.normal()).min(1.0);
        let sin_theta = (1.0 - cos_theta.powi(2)).sqrt();

        let direction = if refraction_ratio * sin_theta > 1.0
            || reflectance(cos_theta, refraction_ratio) > rng.gen()
        {
            reflect(&unit_direction, hit.normal())
        } else {
            refract(&unit_direction, hit.normal(), refraction_ratio)
        };
        *scattered = Ray::new(*hit.point(), direction);
        return true;
    }
}

fn reflectance(cosine: f32, ref_idx: f32) -> f32 {
    // Schlick's approximation
    let r0 = ((1.0 - ref_idx) / (1.0 + ref_idx)).powi(2);
    return r0 + (1.0 - r0) * (1.0 - cosine).powi(5);
}
