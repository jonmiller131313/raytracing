use rgb::RGB;

use crate::{objects::RayHit, ray::Ray, vec3::Vec3};

pub mod dielectric;
pub mod lambertian;
pub mod metal;

pub use dielectric::Dielectric;
pub use lambertian::Lambertian;
pub use metal::Metal;

pub type MaterialPtr = Box<dyn Material + Send + Sync>;

pub trait Material {
    fn scatter(
        &self,
        ray: &Ray,
        hit: &RayHit,
        attenuation: &mut RGB<f32>,
        scattered: &mut Ray,
    ) -> bool;
}

fn reflect(v: &Vec3, n: &Vec3) -> Vec3 {
    *v - 2.0 * v.dot(*n) * *n
}

fn refract(uv: &Vec3, n: &Vec3, etai_over_etat: f32) -> Vec3 {
    let cos_theta = (-*uv).dot(*n).min(1.0);
    let r_out_perp = etai_over_etat * (*uv + cos_theta * *n);
    let r_out_parallel = -(1.0 - r_out_perp.len_squared()).abs().sqrt() * *n;
    return r_out_perp + r_out_parallel;
}
