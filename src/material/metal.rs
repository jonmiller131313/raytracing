use super::{reflect, Material};
use crate::{objects::RayHit, ray::Ray, vec3::Vec3};

use rgb::RGB;

pub struct Metal {
    color: RGB<f32>,
    fuzz: f32,
}
impl Metal {
    pub fn new(color: RGB<f32>, fuzz: f32) -> Self {
        Self { color, fuzz }
    }
}
impl Material for Metal {
    fn scatter(
        &self,
        ray: &Ray,
        hit: &RayHit,
        attenuation: &mut RGB<f32>,
        scattered: &mut Ray,
    ) -> bool {
        let reflected = reflect(&ray.direction().unit(), hit.normal());
        *scattered = Ray::new(*hit.point(), reflected + self.fuzz * Vec3::rand());
        *attenuation = self.color;
        return scattered.direction().dot(*hit.normal()) > 0.0;
    }
}
