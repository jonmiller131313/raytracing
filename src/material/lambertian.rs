use super::Material;
use crate::{objects::RayHit, ray::Ray, vec3::Vec3};

use rgb::RGB;

pub struct Lambertian {
    color: RGB<f32>,
}
impl Lambertian {
    pub fn new(color: RGB<f32>) -> Self {
        Self { color }
    }
}
impl Material for Lambertian {
    fn scatter(
        &self,
        _: &Ray,
        hit: &RayHit,
        attenuation: &mut RGB<f32>,
        scattered: &mut Ray,
    ) -> bool {
        let mut scatter_direction = *hit.normal() + Vec3::rand_unit_vector();
        if scatter_direction.near_zero() {
            scatter_direction = *hit.normal();
        }

        *scattered = Ray::new(*hit.point(), scatter_direction);
        *attenuation = self.color;
        return true;
    }
}
