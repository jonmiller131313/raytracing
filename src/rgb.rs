use std::collections::HashMap;
use std::{
    cell::RefCell,
    fs::File,
    io::{BufWriter, Result as IoResult, Write},
    path::Path,
};

use anyhow::{Context, Result};
use rgb::RGB;

use crate::{utils::get_progress_bar, vec3::Vec3};

pub struct PpmImage(RefCell<BufWriter<File>>);
impl PpmImage {
    pub fn init(file: &Path, width: usize, height: usize) -> Result<Self> {
        let mut fd = BufWriter::new(
            File::options()
                .create(true)
                .write(true)
                .truncate(true)
                .open(file)
                .context("Unable to create/open file")?,
        );
        fd.write("P3\n".as_bytes())
            .context("Unable to write PPM header")?;

        fd.write(format!("{} {}\n", width, height).as_bytes())
            .context("Unable to write image dimensions")?;

        fd.write(format!("{}\n", u8::MAX).as_bytes())
            .context("Unable to write max color size")?;

        return Ok(Self(RefCell::new(fd)));
    }

    pub fn write_pixel(&self, pixel: RGB<f32>, num_samples: usize) -> Result<()> {
        let (r, g, b) = rgb_f32_to_u8(pixel, num_samples).into();
        writeln!(self.0.borrow_mut(), "{r} {g} {b}")?;
        return Ok(());
    }

    pub fn write_pixels(
        &self,
        pixels: HashMap<(usize, usize), RGB<f32>>,
        width: usize,
        height: usize,
        num_samples: usize,
    ) -> Result<()> {
        let progress = get_progress_bar((width * height) as u64, "Saving Image", "Pixel");
        for y in (0..height).rev() {
            for x in 0..width {
                self.write_pixel(pixels[&(x, y)], num_samples)
                    .with_context(|| format!("Unable to write pixel ({x}, {y})"))?;
                progress.inc(1);
            }
        }
        progress.finish();

        Ok(())
    }

    pub fn flush(&self) -> IoResult<()> {
        self.0.borrow_mut().flush()
    }
}

fn rgb_f32_to_u8(rgb: RGB<f32>, num_samples: usize) -> RGB<u8> {
    let (r, g, b) = (256.0
        * ((Vec3::from(rgb) * (1.0 / num_samples as f32))
            .sqrt()
            .clamp(0.0, 1.0)))
    .into();
    return RGB::new(r as u8, g as u8, b as u8);
}
