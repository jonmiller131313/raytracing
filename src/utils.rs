use indicatif::{ProgressBar, ProgressStyle};

pub fn get_progress_bar(len: u64, msg: &str, units: &str) -> ProgressBar {
    ProgressBar::new(len).with_style(
        ProgressStyle::with_template(&format!(
            "[{{elapsed_precise}}] {msg} (~{{eta}}) [{{wide_bar}}] {units} {{pos}} / {{len}}"
        ))
        .expect("Invalid progress bar template")
        .progress_chars("#> "),
    )
}
