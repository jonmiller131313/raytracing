use crate::{
    material::MaterialPtr,
    objects::{HitSurface, RayHit},
    ray::Ray,
    vec3::Point,
};

pub struct Sphere {
    center: Point,
    radius: f32,
    material: MaterialPtr,
}
impl Sphere {
    pub fn new(center: Point, radius: f32, material: MaterialPtr) -> Self {
        Self {
            center,
            radius,
            material,
        }
    }
}
impl HitSurface for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {
        let oc = *(ray.origin()) - self.center;

        let a = ray.direction().len_squared();
        let half_b = oc.dot(*ray.direction());
        let c = oc.len_squared() - self.radius.powi(2);

        let discriminant = half_b.powi(2) - a * c;
        if discriminant < 0.0 {
            return None;
        }

        let mut root = (-half_b - discriminant.sqrt()) / a;
        if root < t_min || root > t_max {
            root = (-half_b + discriminant.sqrt()) / a;
            if root < t_min || root > t_max {
                return None;
            }
        }

        let t = root;
        let point = ray.at(t);
        let normal = (point - self.center) / self.radius;

        let mut hit = RayHit::new(point, normal, t, &self.material);
        hit.set_face_normal(ray, &normal);
        return Some(hit);
    }
}
