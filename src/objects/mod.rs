use std::sync::Arc;

use crate::{
    material::MaterialPtr,
    ray::Ray,
    vec3::{Point, Vec3},
};

pub mod sphere;

pub use sphere::Sphere;

pub trait HitSurface {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit>;
}

pub struct RayHit<'a> {
    point: Point,
    normal: Vec3,
    t: f32,
    is_front: Option<bool>,
    material: &'a MaterialPtr,
}
impl<'a> RayHit<'a> {
    pub fn new(point: Point, normal: Vec3, t: f32, material: &'a MaterialPtr) -> Self {
        Self {
            point,
            normal,
            t,
            is_front: None,
            material,
        }
    }

    pub fn point(&self) -> &Point {
        &self.point
    }

    pub fn normal(&self) -> &Vec3 {
        &self.normal
    }

    pub fn t(&self) -> f32 {
        self.t
    }

    pub fn is_front(&self) -> Option<bool> {
        self.is_front
    }

    pub fn material(&self) -> &MaterialPtr {
        self.material
    }

    pub fn set_face_normal(&mut self, ray: &Ray, outward_normal: &Vec3) {
        self.is_front = Some(ray.direction().dot(*outward_normal) < 0.0);
        if self.is_front.unwrap() {
            self.normal = *outward_normal;
        } else {
            self.normal = -*outward_normal;
        }
    }
}

pub type ObjectPtr = Arc<dyn HitSurface + Sync + Send>;

pub struct Objects(Vec<ObjectPtr>);
impl Objects {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn push(&mut self, object: ObjectPtr) {
        self.0.push(object);
    }
}
impl HitSurface for Objects {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<RayHit> {
        let mut closest_t = t_max;
        let mut closest_hit = None;

        for object in self.0.iter() {
            if let Some(hit) = object.hit(ray, t_min, closest_t) {
                closest_t = hit.t;
                closest_hit = Some(hit);
            }
        }

        return closest_hit;
    }
}
