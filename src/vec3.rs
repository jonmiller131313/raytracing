use std::{iter::repeat_with, ops::*};

use ::rgb::RGB;
use rand::prelude::*;

#[derive(Copy, Clone, Debug)]
pub struct Vec3(f32, f32, f32);
impl Vec3 {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self(x, y, z)
    }

    pub fn x(&self) -> f32 {
        self.0
    }

    pub fn y(&self) -> f32 {
        self.1
    }

    pub fn z(&self) -> f32 {
        self.2
    }

    pub fn len(&self) -> f32 {
        self.len_squared().sqrt()
    }

    pub fn len_squared(&self) -> f32 {
        self.0.powi(2) + self.1.powi(2) + self.2.powi(2)
    }

    pub fn sqrt(self) -> Self {
        let (x, y, z) = self.into();
        return Self(x.sqrt(), y.sqrt(), z.sqrt());
    }

    pub fn clamp(self, min: f32, max: f32) -> Self {
        let (x, y, z) = self.into();
        return Self(x.clamp(min, max), y.clamp(min, max), z.clamp(min, max));
    }

    pub fn unit(self) -> Self {
        self / self.len()
    }

    pub fn dot(self, rhs: Self) -> f32 {
        let (x1, y1, z1) = self.into();
        let (x2, y2, z2) = rhs.into();
        return x1 * x2 + y1 * y2 + z1 * z2;
    }

    pub fn cross(self, rhs: Self) -> Self {
        let (x1, y1, z1) = self.into();
        let (x2, y2, z2) = rhs.into();
        return Self::new(y1 * z2 - y2 * z1, x2 * z1 - x1 * z2, x1 * y2 - x2 * y1);
    }

    pub fn near_zero(&self) -> bool {
        const THRESHOLD: f32 = 1e-5;
        self.0.abs() < THRESHOLD && self.1.abs() < THRESHOLD && self.2.abs() < THRESHOLD
    }

    pub fn rand_in_unit_disk() -> Self {
        let mut rng = thread_rng();
        return repeat_with(|| Self::new(rng.gen_range(-1.0..1.0), rng.gen_range(-1.0..1.0), 0.0))
            .filter(|vec| vec.len_squared() < 1.0)
            .next()
            .expect("Unable to generate vector in unit disk?");
    }

    pub fn rand_range(range: Range<f32>) -> Self {
        let mut rng = thread_rng();
        return Self::new(
            rng.gen_range(range.clone()),
            rng.gen_range(range.clone()),
            rng.gen_range(range),
        );
    }

    // Diffuse methods
    pub fn rand() -> Self {
        let mut rng = thread_rng();

        return repeat_with(|| {
            Self::new(
                rng.gen_range(-1.0..1.0),
                rng.gen_range(-1.0..1.0),
                rng.gen_range(-1.0..1.0),
            )
        })
        .filter(|vec| vec.len_squared() < 1.0)
        .next()
        .expect("Unable to generate vector in unit sphere?");
    }

    pub fn rand_unit_vector() -> Self {
        Self::rand().unit()
    }

    pub fn random_in_hemisphere(normal: &Vec3) -> Self {
        let in_unit_sphere = Self::rand();
        return if in_unit_sphere.dot(*normal) > 0.0 {
            in_unit_sphere
        } else {
            -in_unit_sphere
        };
    }
}

impl Default for Vec3 {
    fn default() -> Self {
        Vec3::new(0.0, 0.0, 0.0)
    }
}
impl From<(f32, f32, f32)> for Vec3 {
    fn from(value: (f32, f32, f32)) -> Self {
        Vec3::new(value.0, value.1, value.2)
    }
}
impl From<Vec3> for (f32, f32, f32) {
    fn from(value: Vec3) -> Self {
        (value.0, value.1, value.2)
    }
}
impl From<RGB<f32>> for Vec3 {
    fn from(value: RGB<f32>) -> Self {
        let (r, g, b) = value.into();
        return Self::new(r, g, b);
    }
}
impl From<Vec3> for RGB<f32> {
    fn from(value: Vec3) -> Self {
        let (x, y, z) = value.into();
        return Self::new(x, y, z);
    }
}

impl Neg for Vec3 {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self(-self.0, -self.1, -self.2)
    }
}

impl Add for Vec3 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let (x1, y1, z1) = self.into();
        let (x2, y2, z2) = rhs.into();
        return Vec3::new(x1 + x2, y1 + y2, z1 + z2);
    }
}
impl Add<RGB<f32>> for Vec3 {
    type Output = Self;

    fn add(self, rhs: RGB<f32>) -> Self::Output {
        self + Self::from(rhs)
    }
}
impl Add<Vec3> for RGB<f32> {
    type Output = Vec3;

    fn add(self, rhs: Vec3) -> Self::Output {
        rhs + self
    }
}
impl Sub for Vec3 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        let (x1, y1, z1) = self.into();
        let (x2, y2, z2) = rhs.into();
        return Vec3::new(x1 - x2, y1 - y2, z1 - z2);
    }
}

impl Mul for Vec3 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let (x1, y1, z1) = self.into();
        let (x2, y2, z2) = rhs.into();
        return Vec3::new(x1 * x2, y1 * y2, z1 * z2);
    }
}
impl Div for Vec3 {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        let (x1, y1, z1) = self.into();
        let (x2, y2, z2) = rhs.into();
        return Vec3::new(x1 / x2, y1 / y2, z1 / z2);
    }
}

impl Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Vec3::new(self.0 * rhs, self.1 * rhs, self.2 * rhs)
    }
}
impl Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, rhs: Vec3) -> Self::Output {
        rhs * self
    }
}
impl Div<f32> for Vec3 {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        Vec3::new(self.0 / rhs, self.1 / rhs, self.2 / rhs)
    }
}
impl Div<Vec3> for f32 {
    type Output = Vec3;

    fn div(self, rhs: Vec3) -> Self::Output {
        rhs * self
    }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
        self.1 += rhs.1;
        self.2 += rhs.2;
    }
}
impl SubAssign for Vec3 {
    fn sub_assign(&mut self, rhs: Self) {
        self.0 -= rhs.0;
        self.1 -= rhs.1;
        self.2 -= rhs.2;
    }
}

impl MulAssign for Vec3 {
    fn mul_assign(&mut self, rhs: Self) {
        self.0 *= rhs.0;
        self.1 *= rhs.1;
        self.2 *= rhs.2;
    }
}
impl DivAssign for Vec3 {
    fn div_assign(&mut self, rhs: Self) {
        self.0 /= rhs.0;
        self.1 /= rhs.1;
        self.2 /= rhs.2;
    }
}

impl MulAssign<f32> for Vec3 {
    fn mul_assign(&mut self, rhs: f32) {
        self.0 *= rhs;
        self.1 *= rhs;
        self.2 *= rhs;
    }
}
impl DivAssign<f32> for Vec3 {
    fn div_assign(&mut self, rhs: f32) {
        self.0 /= rhs;
        self.1 /= rhs;
        self.2 /= rhs;
    }
}

pub type Point = Vec3;
