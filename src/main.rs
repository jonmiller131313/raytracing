use std::path::Path;
use std::sync::Arc;

use ::rgb::RGB;
use anyhow::{Context, Result};
use rand::prelude::*;
use rayon::prelude::*;

use crate::{
    camera::Camera,
    material::*,
    objects::*,
    ray::Ray,
    rgb::PpmImage,
    utils::get_progress_bar,
    vec3::{Point, Vec3},
};

mod camera;
pub mod material;
mod objects;
pub mod ray;
mod rgb;
pub mod utils;
pub mod vec3;

fn main() -> Result<()> {
    let aspect_ratio = 16.0 / 9.0;
    let image_width: usize = 1920;
    let image_height: usize = (image_width as f32 / aspect_ratio).round() as usize;

    let samples_per_pixel = 100;
    let ray_scattering_recursion_max = 50;

    let look_from = Point::new(13.0, 2.0, 3.0);
    let look_at = Point::new(0.0, 0.0, 0.0);
    let vup = Vec3::new(0.0, 1.0, 0.0);
    let vfov = 20.0;
    let aspect_ratio = 16.0 / 9.0;
    let aperture = 0.1;
    let focus_dist = 10.0;

    let camera = Camera::init(
        &look_from,
        &look_at,
        &vup,
        vfov,
        aspect_ratio,
        aperture,
        focus_dist,
    );

    let world = create_scene();

    let image_file = Path::new("image.ppm");

    let progress = get_progress_bar(
        (image_width * image_height) as u64,
        "Rendering Image",
        "Pixel",
    );

    let pixels = (0..image_height)
        .rev()
        .flat_map(|y| (0..image_width).map(move |x| (x, y)))
        .par_bridge()
        .map(|(x, y)| {
            (
                (x, y),
                (0..samples_per_pixel)
                    .map(|_| {
                        let mut rng = thread_rng();
                        (
                            (x as f32 + rng.gen::<f32>()) / (image_width as f32 - 1.0),
                            (y as f32 + rng.gen::<f32>()) / (image_height as f32 - 1.0),
                        )
                    })
                    .map(|(u, v)| camera.get_ray(u, v))
                    .map(|ray| ray_color(&ray, &world, ray_scattering_recursion_max))
                    .sum(),
            )
        })
        .inspect(|_| progress.inc(1))
        .collect();
    progress.finish();

    let image = PpmImage::init(image_file, image_width, image_height)
        .with_context(|| format!("Unable to initialize PPM image: {image_file:?}"))?;
    image
        .write_pixels(pixels, image_width, image_height, samples_per_pixel)
        .context("Unable to write image")?;

    image.flush().context("Unable to flush PPM image")?;

    return Ok(());
}

fn create_scene() -> Objects {
    let mut world = Objects::new();
    let mut rng = thread_rng();

    world.push(Arc::new(Sphere::new(
        Point::new(0.0, -1000.0, 0.0),
        1000.0,
        Box::new(Lambertian::new(RGB::new(0.5, 0.5, 0.5))),
    )));

    for a in -11..11 {
        for b in -11..11 {
            let a = a as f32;
            let b = b as f32;

            let center = Point::new(a + 0.9 * rng.gen::<f32>(), 0.2, b + 0.9 * rng.gen::<f32>());
            if (center - Point::new(4.0, 0.2, 0.0)).len() <= 0.9 {
                continue;
            }

            let material: MaterialPtr = match rng.gen::<f32>() {
                r if r < 0.8 => {
                    let color = RGB::from(Vec3::rand() * Vec3::rand());
                    Box::new(Lambertian::new(color))
                }
                r if r < 0.95 => {
                    let color = RGB::from(Vec3::rand_range(0.5..1.0));
                    let fuzz = rng.gen_range(0.0..0.5);
                    Box::new(Metal::new(color, fuzz))
                }
                _ => Box::new(Dielectric::new(1.5)),
            };

            world.push(Arc::new(Sphere::new(center, 0.2, material)));
        }
    }

    let big_material_1 = Box::new(Dielectric::new(1.5));
    world.push(Arc::new(Sphere::new(
        Point::new(0.0, 1.0, 0.0),
        1.0,
        big_material_1,
    )));

    let big_material_2 = Box::new(Lambertian::new(RGB::new(0.4, 0.2, 0.1)));
    world.push(Arc::new(Sphere::new(
        Point::new(-4.0, 1.0, 0.0),
        1.0,
        big_material_2,
    )));

    let big_material_3 = Box::new(Metal::new(RGB::new(0.7, 0.6, 0.5), 0.0));
    world.push(Arc::new(Sphere::new(
        Point::new(4.0, 1.0, 0.0),
        1.0,
        big_material_3,
    )));

    return world;
}

fn ray_color(ray: &Ray, world: &Objects, recursion_depth: usize) -> RGB<f32> {
    if recursion_depth == 0 {
        return RGB::new(0.0, 0.0, 0.0);
    }
    if let Some(hit) = world.hit(ray, 0.001, f32::INFINITY) {
        let mut scattered = Ray::new(Point::new(0.0, 0.0, 0.0), Vec3::new(0.0, 0.0, 0.0));
        let mut attenuation = RGB::new(0.0, 0.0, 0.0);

        if hit
            .material()
            .scatter(ray, &hit, &mut attenuation, &mut scattered)
        {
            return attenuation * ray_color(&scattered, world, recursion_depth - 1);
        }
        return RGB::new(0.0, 0.0, 0.0);
    }
    let unit_direction = ray.direction().unit();
    let t = 0.5 * (unit_direction.y() + 1.0);
    return RGB::new(1.0, 1.0, 1.0) * (1.0 - t) + RGB::new(0.5, 0.7, 1.0) * t;
}
